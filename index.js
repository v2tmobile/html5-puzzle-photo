/**
 * Created by thuydv on 03/12/2015.
 */
//Configure a few settings and attach camera
Webcam.set({
    // live preview size
    width: 200,
    height: 150,

    // device capture size
    dest_width: 200,
    dest_height: 150,

    // final cropped size
    crop_width: 200,
    crop_height: 150,

    // format and quality
    image_format: 'jpeg',
    jpeg_quality: 90,

    // flip horizontal (mirror mode)
    flip_horiz: false
});
Webcam.attach( '#my_camera0' );
Webcam.attach( '#my_camera1' );
Webcam.attach( '#my_camera2' );
Webcam.attach( '#my_camera3' );
Webcam.attach( '#my_camera4' );
Webcam.attach( '#my_camera5' );
Webcam.attach( '#my_camera6' );
Webcam.attach( '#my_camera7' );
Webcam.attach( '#my_camera8' );
Webcam.attach( '#my_camera9' );
Webcam.attach( '#my_camera10' );
Webcam.attach( '#my_camera11' );
Webcam.attach( '#my_camera12' );
Webcam.attach( '#my_camera13' );
Webcam.attach( '#my_camera14' );
Webcam.attach( '#my_camera15' );
//chụp ảnh
$('.photo_booth').click(function (){
    var element = $(this).find('.my_photo_booth');
    Webcam.snap( function(data_uri) {
        element.html('<img class="img_camera" src="'+data_uri+'"/>');
    } );
});

//canvas id c
var canvas = new fabric.Canvas('canvas_page_one', {
    width:800,
    height:600
});
fabric.Image.fromURL('oImg/Heart_clip_art.svg', function(oImg) {
    oImg.set({
        selectable: false,
        width:800,
        height:600
    });
    canvas.add(oImg);
});
$('#fabric_oImg_one').click(function(){
    canvas.clear().renderAll();
    fabric.Image.fromURL('oImg/Heart_clip_art.svg', function(oImg) {
        oImg.set({
            selectable: false,
            width:800,
            height:600
        });
        canvas.add(oImg);
    });
});
$('#fabric_oImg_two').click(function(){
    canvas.clear().renderAll();
    fabric.Image.fromURL('oImg/hinh2.svg', function(oImg) {
        oImg.set({
            selectable: false,
            width:800,
            height:600
        });
        canvas.add(oImg);
    });
});
$('#fabric_oImg_three').click(function(){
    canvas.clear().renderAll();
    fabric.Image.fromURL('oImg/hinh3.svg', function(oImg) {
        oImg.set({
            selectable: false,
            width:800,
            height:600
        });
        canvas.add(oImg);
    });
});
$('#fabric_oImg_four').click(function(){
    canvas.clear().renderAll();
    fabric.Image.fromURL('oImg/hinh3.svg', function(oImg) {
        oImg.set({
            selectable: false,
            width:800,
            height:600
        });
        canvas.add(oImg);
    });
});
$('#fabric_oImg_five').click(function(){
    canvas.clear().renderAll();
    fabric.Image.fromURL('oImg/hinh3.svg', function(oImg) {
        oImg.set({
            selectable: false,
            width:800,
            height:600
        });
        canvas.add(oImg);
    });
});
$('#fabric_oImg_six').click(function(){
    canvas.clear().renderAll();
    fabric.Image.fromURL('oImg/hinh3.svg', function(oImg) {
        oImg.set({
            selectable: false,
            width:800,
            height:600
        });
        canvas.add(oImg);
    });
});
$('#fabric_oImg_seven').click(function(){
    canvas.clear().renderAll();
    fabric.Image.fromURL('oImg/hinh3.svg', function(oImg) {
        oImg.set({
            selectable: false,
            width:800,
            height:600
        });
        canvas.add(oImg);
    });
});
$('#fabric_oImg_eight').click(function(){
    canvas.clear().renderAll();
    fabric.Image.fromURL('oImg/hinh3.svg', function(oImg) {
        oImg.set({
            selectable: false,
            width:800,
            height:600
        });
        canvas.add(oImg);
    });
});
$('#fabric_oImg_nine').click(function(){
    canvas.clear().renderAll();
    fabric.Image.fromURL('oImg/hinh3.svg', function(oImg) {
        oImg.set({
            selectable: false,
            width:800,
            height:600
        });
        canvas.add(oImg);
    });
});
//click through canvas
$('.canvas-overlay').live('click', function (e) {
    var mouseX = e.pageX;
    var mouseY = e.pageY;
    $('.photo_booth').each(function() {
        var offset = $(this).offset();
        var width = $(this).width();
        var height = $(this).height();
        if (mouseX > offset.left && mouseX < offset.left+width && mouseY > offset.top && mouseY < offset.top+height) {
            if ($(this).find('.img_camera').length) {
                var camera = $(this).closest('.photo_booth').find('.my_photo_booth').attr('id');
                Webcam.attach(camera);
            } else {
                var element = $(this).find('.my_photo_booth');
                Webcam.snap( function(data_uri) {
                    element.html('<img class="img_camera" src="'+data_uri+'"/>');
                });
            }
        }
    });
});
// hide show canvas
$( document ).ready(function() {
    var canvas;
    $('#exit').hide();
    $('#exit_right').hide();
    $("#function").hide();
    $("#footer").hide();
    $('#next_page_two').click(function(){

        $('.photo_booth').each(function() {
                if ($(this).find('.img_camera').length) {
                } else {
                    var element = $(this).find('.my_photo_booth');
                    Webcam.snap( function(data_uri) {
                        element.html('<img class="img_camera" src="'+data_uri+'"/>');
                    });
            }
        });

        $('.canvas-container').hide();
        $('#next_page_two').hide();
        $('#exit').show();
        $('.photo_booth').hide();
        $('#main').hide();
        $('#img_left_bottom').hide();
        $("#exit_right").show();
        $(".introduction").hide();
        $("#fabric_oImg").hide();
        $("#text_fabric_oImg").hide();
        $("#comment").hide();
        $("#function").show();
        $('#matrix').hide();

        $('#introduction_exit').click(function(){
            $('#icon').show();
            $("#matrix").hide();
        });
        $('#hieuung').click(function(){
            $('#matrix').show();
            $("#icon").hide();
        });
            var i = 1;
            var j = 1;
            var width = 200;
            var height = 150;
            canvas=new fabric.Canvas('canvas_page_two', {width: 800, height: 600});
            $('.img_camera').each(function() {
                console.log(i.toString() + "|" + j.toString());
                var element=$(this).attr('src');
                fabric.Image.fromURL(element, function (imgObj) {
                    //imgObj.set("angle", "-180").set('flipY', true);
                    canvas.add(imgObj).renderAll();
                }, {
                    top: j*height - height,
                    left: i* width - width,
                    width: width,
                    height: height,
                    flipX: true,
                    selectable: false,
                    type: "user-photo"
                });

                i++;
                if (i > 4) {
                    i = 1;
                    j++;
                }
            });
        //đưa ảnh icon vào.lấy địa chỉ url và đưa vào canvas
        $(".icon_noel").click(function(){
                var element=$(this).attr('src');
                fabric.Image.fromURL(element, function(oImg) {
                    canvas.add(oImg);
            });
        });

            $(".filters_1").live('click', function(){
                //console.log($(this).attr('id'));
                var effectName = $(this).attr('id');
                canvas.getObjects().forEach(function(canvasElement) {
                    if (canvasElement.type == "user-photo") {
                        canvasElement.filters = [];
                        canvasElement.applyFilters(canvas.renderAll.bind(canvas));
                        switch (effectName) {
                            case 'hieu_ung_1':
                                canvasElement.filters.push(new fabric.Image.filters.ColorMatrix({
                                    matrix: [0.6279345635605994, 0.3202183420819367, -0.03965408211312453, 0, 9.651285835294123,
                                        0.02578397704808868, 0.6441188644374771, 0.03259127616149294, 0, 7.462829176470591,
                                        0.0466055556782719, -0.0851232987247891, 0.5241648018700465, 0, 5.159190588235296,
                                        0, 0, 0, 1, 0]
                                }));
                                break;
                            case 'hieu_ung_2':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [
                                        1.1285582396593525, -0.3967382283601348, -0.03992559172921793, 0, 63.72958762196502,
                                        -0.16404339962244616, 1.0835251566291304, -0.05498805115633132, 0, 24.732407896706203,
                                        -0.16786010706155763, -0.5603416277695248, 1.6014850761964943, 0, 35.62982807460946,
                                        0, 0, 0, 1, 0
                                    ]
                                }));
                                break;
                            case 'hieu_ung_3':
                                canvasElement.filters.push(
                                    new fabric.Image.filters.Sepia(),
                                    new fabric.Image.filters.Brightness({ brightness: 100 }));
                                break;
                            case 'hieu_ung_4':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [1,0,0,0.4,0,  /* Red */
                                        0,1,0,0,0,   /* Green */
                                        0,0,0,0,0,   /* Blue */
                                        0,0,0,1,0]
                                }));
                                break;
                            case 'hieu_ung_5':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [2,0,0,0,0,   /* Red */
                                        0,1,0,0,0,   /* Green */
                                        0,0,1,0,0,   /* Blue */
                                        0,0,0,1,0 ]
                                }));
                                break;
                            case 'hieu_ung_6':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [0,0,0,0,80,  /* Red */
                                        0,1,0,0,0,   /* Green */
                                        0,0,0,0,0,   /* Blue */
                                        0,0,0,1,0 ]
                                }));
                                break;
                            case 'hieu_ung_7':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [0,0,0,0,255, /* Red */
                                        0,1,0,0,0,   /* Green */
                                        0,0,0,0,0,   /* Blue */
                                        0,0,0,1,0 ]
                                }));
                                break;
                            case 'hieu_ung_8':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [1,0,0,0.4,0,  /* Red */
                                        0,1,0,0,0,   /* Green */
                                        0,0,0,0,0,   /* Blue */
                                        0,0,0,1,0 ]
                                }));
                                break;
                            case 'hieu_ung_10':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [0,0,1,0,0,   /* Red */
                                        1,0,0,0,0,   /* Green */
                                        0,1,0,0,0,   /* Blue */
                                        0,0,0,1,0 ]
                                }));
                                break;
                            case 'hieu_ung_11':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [1,0,0,0,0,   /* Red */
                                        1,0,0,0,0,   /* Green */
                                        1,0,0,0,0,   /* Blue */
                                        0,0,0,1,0 ]
                                }));
                                break;
                            case 'hieu_ung_12':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [1,0,0,0,50,  /* Red */
                                        0,1,0,0,50,   /* Green */
                                        0,0,1,0,50,   /* Blue */
                                        0,0,0,1,0]
                                }));
                                break;
                            case 'hieu_ung_13':
                                canvasElement.filters.splice(0, 1, new fabric.Image.filters.ColorMatrix({
                                    matrix: [1,0,0,0,-50,  /* Red */
                                        0,1,0,0,-50,   /* Green */
                                        0,0,1,0,-50,   /* Blue */
                                        0,0,0,1,0 ]
                                }));
                                break;
                            case 'remove':
                                canvasElement.filters.length = 0;
                                break;
                            default:
                                break;
                        }
                        //if (effectName === 'hieu_ung_1') {
                        //    canvasElement.filters.push(new fabric.Image.filters.Grayscale());
                        //}

                        //canvasElement.filters.push(new fabric.Image.filters.Grayscale());
                        canvasElement.applyFilters(canvas.renderAll.bind(canvas));
                    }

                });
            });

            $("#exit_text").click(function(){
                canvas.add(new fabric.IText('Tap and Type', {
                    fontStyle: 'italic',
                    fontFamily: 'Hoefler Text',
                    textBackgroundColor: "#80E580",
                    left: 400,
                    top: 100 ,
                }));
            });

        $("#next_page_one").click(function(){
            function download(url,name){
// make the link. set the href and download. emulate dom click
                $('<a>').attr({href:url,download:name})[0].click();
            }
            function downloadFabric(canvas,name){
                //  convert the canvas to a data url and download it.
                download(canvas.toDataURL(),name+'.png');
            }
           // downloadFabric(canvas,'<file name>');
            $("#footer").show();
            $("#container").hide();
            var abc=new fabric.Canvas('canvas_page_three', {width: 808, height: 608});
            fabric.Image.fromURL(canvas.toDataURL(), function(img) {
                img.set({
                    selectable: false
                });
                abc.add(img);
            });
        });

        $("#btn-download").click(function(){
            function download(url,name){
// make the link. set the href and download. emulate dom click
                $('<a>').attr({href:url,download:name})[0].click();
            }
            function downloadFabric(canvas,name){
                //  convert the canvas to a data url and download it.
                download(canvas.toDataURL(),name+'.png');
            }
             downloadFabric(canvas,'camera_v2t');
        });
        // Post a BASE64 Encoded PNG Image to facebook
        function PostImageToFacebook(authToken) {
            var canvas = document.getElementById("canvas_page_three");
            var imageData = canvas.toDataURL("image/png");
            try {
                blob = dataURItoBlob(imageData);
            } catch (e) {
                console.log(e);
            }
            //lấy text trong input
            var thuy=$('#text_post').val();
            //
            var text=thuy +' \n ' + 'Tại https://apps.v2t.mobi/web/html5-puzzle-photo/';
            console.log(thuy);
            var fd = new FormData();
            fd.append("access_token", authToken);
            fd.append("source", blob);
            fd.append("message",text);
            try {
                $.ajax({
                    url: "https://graph.facebook.com/me/photos?access_token=" + authToken,
                    type: "POST",
                    data: fd,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        console.log("success " + data);
                        $("#poster").html("Posted Canvas Successfully");
                    },
                    error: function (shr, status, data) {
                        console.log("error " + data + " Status " + shr.status);
                    },
                    complete: function () {
                        console.log("Posted to facebook");
                    }
                });

            } catch (e) {
                console.log(e);
            }
        }

// Convert a data URI to blob
        function dataURItoBlob(dataURI) {
            var byteString = atob(dataURI.split(',')[1]);
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ab], {
                type: 'image/png'
            });
        }

        /**
         * Created by thuydv on 1/7/2016.
         */
        $("#shareBtn").live('click',function() {
            console.log(canvas.toDataURL());
            //FB.ui({ method : 'feed',
            //    //message: 'http://suckhoedoisong.vn/thumb_600x400/Images/_OLD/JRGSJiLd3e5GsxdM0P2pqg65KoKccc/Image/2013/11/de-phong-di-ung-thuc-an-29e94.jpg',
            //    //caption:  'http://suckhoedoisong.vn/thumb_600x400/Images/_OLD/JRGSJiLd3e5GsxdM0P2pqg65KoKccc/Image/2013/11/de-phong-di-ung-thuc-an-29e94.jpg',
            //    link   : 'http://media.tiin.vn/medias/4f5873c10f715/2012/07/31/d03b91cc-6eb7-47c6-a3ef-bfdfac53ab28.jpg' ,
            //    picture:'http://media.tiin.vn/medias/4f5873c10f715/2012/07/31/d03b91cc-6eb7-47c6-a3ef-bfdfac53ab28.jpg'
            //});
            FB.login(function(response) {
                if (response.authResponse) {
                    var access_token =   FB.getAuthResponse()['accessToken'];
                    console.log(access_token);
                    PostImageToFacebook(access_token);
                } else {
                    //console.log('User cancelled login or did not fully authorize.');
                }
            }, {scope: 'publish_actions'});

        });
    });
});
$('#reload').click(function(){
    location.reload(true);
});